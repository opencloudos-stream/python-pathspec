Summary:        Utility library for gitignore style pattern matching of file paths
Name:           python-pathspec
Version:        0.11.2
Release:        3%{?dist}
License:        MPLv2.0
URL:            https://github.com/cpburnz/python-path-specification
Source0:        https://files.pythonhosted.org/packages/source/p/pathspec/pathspec-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%description
Path Specification (pathspec) is a utility library for pattern matching of file
paths. So far this only includes Git's wildmatch pattern matching which itself
is derived from Rsync's wildmatch. Git uses wildmatch for its gitignore files.

%package -n     python3-pathspec
Summary:        %{summary}

%description -n python3-pathspec
Path Specification (pathspec) is a utility library for pattern matching of file
paths. So far this only includes Git's wildmatch pattern matching which itself
is derived from Rsync's wildmatch. Git uses wildmatch for its gitignore files.


%prep
%autosetup -n pathspec-%{version}

%build
%py3_build

%install
%py3_install


%check
%{__python3} setup.py test

%files -n python3-pathspec
%license LICENSE
%doc README.rst
%{python3_sitelib}/pathspec
%{python3_sitelib}/pathspec-%{version}-py%{python3_version}.egg-info

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.11.2-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.11.2-2
- Rebuilt for loongarch release

* Fri Sep 22 2023 Miaojun Dong <zoedong@tencent.com> - 0.11.2-1
- Bump version to 0.11.2

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.10.2-5
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.10.2-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.10.2-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.10.2-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Dec 1 2022 cunshunxia <cunshunxia@tencent.com> - 0.10.2-1
- initial build
